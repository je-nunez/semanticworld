package mainapp


import java.io._
import java.net.URL

import scala.collection.mutable.{ListBuffer, Map => MutableMap, WrappedArray}
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._

import scopt.OptionParser

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{SQLContext, DataFrame, Row, DataFrameWriter}
import org.apache.spark.sql.{Column, SaveMode}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.storage.StorageLevel

// Stanford CoreNLP: English syntactical analysis of the user reviews
import edu.stanford.nlp.simple.{Document => NLPDocument}

// MIT Java Interface to Princenton WordNet (JWI): semantic analysis of the user reviews
import edu.mit.jwi.RAMDictionary
import edu.mit.jwi.morph.WordnetStemmer
import edu.mit.jwi.item.{POS => WordNetPOS}     // JWI WordNet Part-of-Speech
import edu.mit.jwi.item.{Pointer => WordNetPointer}
import edu.mit.jwi.data.ILoadPolicy

// MongoDB related imports: this is from the MongoDB Java client library
import org.bson.{BsonDocument, Document}

import com.mongodb.MongoClient
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.{Filters, Aggregates, Projections}
import com.mongodb.client.model.Projections._

// This is from the MongoDB Spark connector
import com.mongodb.spark._
import com.mongodb.spark.sql._
import com.mongodb.spark.config.{ReadConfig, WriteConfig}



object SemanticWorld {

  // the command-line parameters this program accepts
  case class CmdLineParams(
      businessName: String = "",     // the business name, if running in business mode
      customerDesire: String = ""      // the customer desire, if running in customer-query mode
  )

  // This is the threshold-value of Yelp reviews below which we ignore the reviews.
  // (See comment below for more analysis.)
  // (This threshold value could also be a command-line parameter in a fully-fledged
  //  program.)

  val ignoreReviewsBelowStars = 4

  def readCommandLineParameters(cmdLineArgs: Array[String]): CmdLineParams = {
    val defaultParams = CmdLineParams()

    val parser = new OptionParser[CmdLineParams]("SemanticWorld") {
      head("SemanticWorld: query the Yelp business reviews by syntactic and semantic analysis.")
      opt[String]("business").abbr("b")
        .text("insights for a business extracted from its customer reviews")
        .action((x, c) => c.copy(businessName = x))
      opt[String]("customer").abbr("c")
        .text("query the Yelp reviews trying to find a business which could satisfy a desire.")
        .action((x, c) => c.copy(customerDesire = x))
      note(
        """
          |Run a syntactical and semantical analysis using Stanford CoreNLP and
          |MIT/Princenton WordNet on Apache Spark with the Yelp business reviews in a MongoDB
          |database:
          |
          |  -b 'business name'
          |  --business 'business name': extracts insights for a business from its customer reviews
          |
          |  -c 'customer request'
          |  --customer 'customer request': query the Yelp reviews trying to find a business which
          |                                 could satisfy a desire (only one word in this version)
        """.stripMargin)
    }

    val params = parser.parse(cmdLineArgs, defaultParams) match {
      case Some(realCmdLineParameters) => realCmdLineParameters
      case _ => defaultParams
    }

    if (params.businessName == "" && params.customerDesire == "") {
      // no option, --business nor --customer, was set in the command-line
      System.err.println(s"""Either one of the options --business or --customer must be given:
                            |'${params.businessName}' and '${params.customerDesire}'""".stripMargin)
      sys.exit(1)
    } else if (params.businessName != "" && params.customerDesire != "") {
      // both options, --business and --customer, were set in the command-line
      System.err.println(s"""Only one of the options --business or --customer must be given:
                            |'${params.businessName}' and '${params.customerDesire}'""".stripMargin)
      sys.exit(1)
    } else if (params.businessName == "" && params.customerDesire.trim == "") {
      // no business name in Yelp given, only a customer desire but it is empty after trimming
      System.err.println("The customer's desire cannot consist only in spaces")
      sys.exit(1)
    }

    params
  }

  def main(cmdLineArgs: Array[String]) : Unit = {

    val cmdLineValues = readCommandLineParameters(cmdLineArgs)

    // for the explanation of some SparkConf settings below, see here:
    //    https://spark.apache.org/docs/1.6.2/configuration.html

    val conf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("Whisper")
      .set("spark.app.id", "SemanticReview")
      .set("spark.executor.heartbeatInterval", "30s")
      .set("spark.driver.maxResultSize", "2g")
      .set("spark.ui.enabled", "false")
      .set("spark.mongodb.input.uri", "mongodb://localhost:27017/")
      .set("spark.mongodb.input.database", "yelp")
      .set("spark.mongodb.input.collection", "review")
      .set("spark.mongodb.output.uri", "mongodb://localhost:27017/")
      .set("spark.mongodb.output.database", "je_nunez_temp_db")
      .set("spark.mongodb.output.collection", "je_nunez_temp_collection")

    val sc = new SparkContext(conf)

    // open the WordNet semantic dictionary
    getOrCreateWordnetDict(forceLoad=true)

    // Only choose those reviews whose truth other readers agree, i.e., other readers give star to
    // these reviews. So we use the star-classification as the truth-assurance of the reviews, ie.,
    // the written review is not a lie. Another way to ensure the truth-value of a reviews is by
    // its "votes.useful" field, instead of its "stars" field: only consider a truthful those
    // reviews whose "votes.useful" is greater than a minimumThresholdValue. These different views
    // of the source data are simply defined by the line containing "Filters" below: if you want
    // to analyze a different input-set as the truth for you, just change the Filters line below.
    // (I also thought in a geographic-localized version, i.e., you query only the input reviews
    // of a business that is close to your location, but this is a very similar case, just using
    // MongoDB geospatial query operators:
    //
    //   https://docs.mongodb.com/manual/reference/operator/query-geospatial/
    //
    // on the Yelp businesses collection in MongoDB which has the geographical location of each
    // business, and then doing an equi-join of this filtered business dataframe with the reviews
    // dataframe for only these businesses near to that geographical location. The manual page for
    // DataFrame in Spark has an example of how to equi-join two dataframes (like if it is an
    // equi-join between two SQL tables):
    //
    //   https://spark.apache.org/docs/1.6.2/api/java/org/apache/spark/sql/DataFrame.html

    val conditionStars = Array(Aggregates.`match`(Filters.gt("stars", ignoreReviewsBelowStars)))
    val mostStarredReviews = sc.loadFromMongoDB()
                               .withPipeline(conditionStars)
                               .toDF()

    val textAnalysisDF = doTextualAnalysis(mostStarredReviews)

    if (cmdLineValues.businessName != "") {

       getInsightsForBusiness(cmdLineValues.businessName, textAnalysisDF)

    } else {

       satisfyACustomerRequest(cmdLineValues.customerDesire, textAnalysisDF)
    }



    /*
      val mostStarredReviewsForThisBusiness = textAnalysisDF.filter(
                    textAnalysisDF("business_id").eqNullSafe(yelpBusinessId)
                 )

      mostStarredReviewsForThisBusiness.persist(StorageLevel.MEMORY_ONLY)

      mostStarredReviewsForThisBusiness.unpersist(blocking=false)
    }
    */

    sc.stop()

  }



  def doTextualAnalysis(yelpReviewsDF: DataFrame): DataFrame = {

    import yelpReviewsDF.sqlContext.implicits._

    // it is defined below this function "semanticTopics" which is called by Spark on each
    // Yelp-review record in the Spark dataframe

    val semanticTopicsDF = yelpReviewsDF
      .select('business_id, 'text, semanticTopics('text).as('topics))

    semanticTopicsDF       // return the new DataFrame

  }


  @transient private var wordnetDictionary: RAMDictionary = _

  @transient private var wordnetStemmer: WordnetStemmer = _

  private def getOrCreateWordnetDict(forceLoad: Boolean = true): RAMDictionary = {
    if (wordnetDictionary == null) {
      // this assumes that the JWI WordNet dictionary is under "./data/dict/" of the directory
      // where you run "sbt run". If not, it will raise an exception, not able to find the WordNet
      // dictionary. The WordNet dictionary is at:
      //         https://wordnet.princeton.edu/wordnet/download/current-version/
      // e.g., for version 3.1:
      //         http://wordnetcode.princeton.edu/wn3.1.dict.tar.gz
      val dirDbWordNet = "data" + File.separator + "dict"
        // "/Users/joseemilio.nunezmaya/projects/RTDS/ibm_hackathon/code/spark_corenlp/data/dict"

      val pathWordNetDb = new URL("file", null, dirDbWordNet)

      wordnetDictionary = new RAMDictionary(pathWordNetDb, ILoadPolicy.BACKGROUND_LOAD)
      wordnetDictionary.open()

      wordnetStemmer = new WordnetStemmer(wordnetDictionary)
    }
    if (forceLoad && ! wordnetDictionary.isLoaded) {
      wordnetDictionary.load(true)
    }
    wordnetDictionary
  }


  // FIX: Remove the "sentence" and "coreNlpPOS" fields, which are for debugging only
  case class MeaningfulRelationship(
    source: String,
    sourceLemma: String,
    sourceIndex: Int,
    relation: String,
    target: String,
    targetLemma: String,
    targetIndex: Int,
    sentence: String = "",
    coreNlpPOS: String = ""
  )

  // The mapping between Part-of-Speech Tags from Stanford CoreNLP (which is used in the
  // syntactic analysis in this program):
  //      The Penn Treebank Project: http://www.cis.upenn.edu/~treebank/
  //      http://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
  // to MIT Java WordNet Interface (which is used in the semantic analysis in this program):
  //      http://projects.csail.mit.edu/jwi/api/edu/mit/jwi/item/POS.html

  private val mapPOSStanford2MIT: Map[String, WordNetPOS] = Map(

    "NN" -> WordNetPOS.NOUN,
    "NNS" -> WordNetPOS.NOUN,

    "JJ" -> WordNetPOS.ADJECTIVE,
    "JJR" -> WordNetPOS.ADJECTIVE,
    "JJS" -> WordNetPOS.ADJECTIVE,

    "RB" -> WordNetPOS.ADVERB,
    "RBR" -> WordNetPOS.ADVERB,
    "RBS" -> WordNetPOS.ADVERB,

    "VB" -> WordNetPOS.VERB,
    "VBD" -> WordNetPOS.VERB,
    "VBP" -> WordNetPOS.VERB,
    "VBZ" -> WordNetPOS.VERB
  )

  private def getPOS4Stanford2MIT(stanfordPOS: String): Option[WordNetPOS] = {
    if (mapPOSStanford2MIT.contains(stanfordPOS)) Some(mapPOSStanford2MIT(stanfordPOS)) else None
  }

  private def isPOSSemantNoun(semanticPOS: Option[WordNetPOS]): Boolean = {
    semanticPOS.isDefined && (semanticPOS.get == WordNetPOS.NOUN)
  }

  private def isPOSSemantAdjective(semanticPOS: Option[WordNetPOS]): Boolean = {
    semanticPOS.isDefined && (semanticPOS.get == WordNetPOS.ADJECTIVE)
  }

  private def isPOSSemantAdverb(semanticPOS: Option[WordNetPOS]): Boolean = {
    semanticPOS.isDefined && (semanticPOS.get == WordNetPOS.ADVERB)
  }

  private def isPOSSemantVerb(semanticPOS: Option[WordNetPOS]): Boolean = {
    semanticPOS.isDefined && (semanticPOS.get == WordNetPOS.VERB)
  }

  // This is the internal processing of the Apache Spark's User-Defined Function (UDF) that
  // will be defined below.

  private def innerSemanticTopics(document: String): List[MeaningfulRelationship] = {

    val semanticDict = getOrCreateWordnetDict()

    def getWordStem(word: String, wordnetPOS: Option[WordNetPOS], corenlpPOS: String): String = {
      try {
        if (wordnetPOS.isDefined && word != "") {
          val stems = wordnetStemmer.findStems(word, wordnetPOS.get)
          if (stems != null && stems.size >= 1) stems.get(0) else  ""   // word + "+" + corenlpPOS
        } else if (List("NNP", "NNPS").contains(corenlpPOS)) {
          // If syntactically it has the Part-of-Speech of a proper name (NNPS*), like Lennon, then
          // don't find its stem and return the word (the proper name) as-it-is:
          word
        } else {
          // word + "++" + corenlpPOS                     // for debugging
          ""
        }
      } catch {
        case exc: java.lang.IllegalArgumentException => ""
      }
    }

    def getLemma(wordStem: String, wordnetPOS: Option[WordNetPOS]): String = {
      if (wordnetPOS.isDefined && wordStem != "") {
        val idxWord = semanticDict.getIndexWord(wordStem.toLowerCase, wordnetPOS.get)
        if ( idxWord != null ) {
          val wordId = idxWord.getWordIDs().get(0)
          if ( wordId != null ) {
             val word = semanticDict.getWord(wordId)
             if ( word != null ) {
               word.getLemma
             } else {
               // wordStem + "-" + wordnetPOS.get       // for debugging
               ""
             }
          } else {
            // wordStem + "-" + wordnetPOS.get          // for debugging
            ""
          }
        } else {
          // wordStem + "-" + wordnetPOS.get            // for debugging
          ""
        }
      } else {
        // wordStem + "-" + "NoPOS"                     // for debugging
        ""
      }
    }


    var semanticTopics = new ListBuffer[MeaningfulRelationship]()

    val sentences = new NLPDocument(document).sentences()

    for { sentence <- sentences } {
       val sentenceStr = sentence.text
       val sentencePOSTags = sentence.posTags()

       def getPOSTag(idxCoreNLP: Int): String = {
         if (idxCoreNLP >= 1) {
           val idx0based = idxCoreNLP - 1
           if (idx0based < sentencePOSTags.length) sentencePOSTags(idx0based) else "INFINITE"
         } else {
           "00"
         }
       }

       val dependGraph = sentence.dependencyGraph()
       val edgesRelationshGraph = dependGraph.edgeListSorted()

       val newSemantTopic = edgesRelationshGraph map {
         case edgeRelation if edgeRelation.getWeight == 1.0 => {

           // get the relation represented by this edge
           val relationType = edgeRelation.getRelation.toString

           // get the word-indexes in the sentence of the source
           // and destination words in this relationship

           val relationSrcIndex = edgeRelation.getSource.index()
           val relationTargIndex = edgeRelation.getTarget.index()
           // get the actual words
           val relationSrcWord = edgeRelation.getSource.word()
           val relationTargWord = edgeRelation.getTarget.word()

           // Part-Of-Speech Tags according to the syntactic analysis (Stanford CoreNLP)
           val tagPOSSrc = getPOSTag(relationSrcIndex)
           val tagPOSTarg = getPOSTag(relationTargIndex)

           // Part-Of-Speech Tags according to the semantic analysis (MIT/Princenton WordNet)
           val tagWNPOSSrc = getPOS4Stanford2MIT(tagPOSSrc)
           val tagWNPOSTarg = getPOS4Stanford2MIT(tagPOSTarg)

           // for the semantic analysis, we deal first with the lemmas of the words, not with the
           // actual words themselves as written by the user in the review. We find the words
           // lemmas and stems from WordNet:
           // Stems of the words according to WordNet in this Part-of-Speech usage of these words:
           var stemSrc = getWordStem(relationSrcWord,
                                     getPOS4Stanford2MIT(tagPOSSrc), tagPOSSrc)
           var stemTarg = getWordStem(relationTargWord,
                                      getPOS4Stanford2MIT(tagPOSTarg), tagPOSTarg)


           def dealWithNSubjSyntacticRelation: MeaningfulRelationship = {

             if (isPOSSemantAdjective(tagWNPOSSrc) && isPOSSemantNoun(tagWNPOSTarg)) {
               // convert the stems of the source and target to their unique lemma base
               val semantSrc = getLemma(stemSrc, tagWNPOSSrc)
               val semantTarg = getLemma(stemTarg, tagWNPOSTarg)

               MeaningfulRelationship(
                 relationSrcWord, semantSrc, relationSrcIndex,
                 relationType,
                 relationTargWord, semantTarg, relationTargIndex,
                 sentenceStr, sentencePOSTags.mkString("-")
               )

             } else {
               // by default, if this is not one of the cases above, this program discards this
               // syntactical relationship
               MeaningfulRelationship("", "", 0, "", "", "", 0, "", "")
             }
           }

           def dealWithAModSyntacticRelation: MeaningfulRelationship = {

             if (isPOSSemantNoun(tagWNPOSSrc) && isPOSSemantAdjective(tagWNPOSTarg)) {
               // convert the stems of the source and target to their unique lemma base
               val semantSrc = getLemma(stemSrc, tagWNPOSSrc)
               val semantTarg = getLemma(stemTarg, tagWNPOSTarg)

               MeaningfulRelationship(
                 relationSrcWord, semantSrc, relationSrcIndex,
                 relationType,
                 relationTargWord, semantTarg, relationTargIndex,
                 sentenceStr, sentencePOSTags.mkString("-")
               )
             } else {
               // by default, if this is not one of the cases above, this program discards this
               // syntactical relationship
               MeaningfulRelationship("", "", 0, "", "", "", 0, "", "")
             }
           }

           def dealWithCompoundSyntacticRelation: MeaningfulRelationship = {

             if (isPOSSemantNoun(tagWNPOSSrc) && isPOSSemantNoun(tagWNPOSTarg)) {
               // convert the stems of the source and target to their unique lemma base
               val semantSrc = getLemma(stemSrc, tagWNPOSSrc)
               val semantTarg = getLemma(stemTarg, tagWNPOSTarg)

               MeaningfulRelationship(
                 relationSrcWord, semantSrc, relationSrcIndex,
                 relationType,
                 relationTargWord, semantTarg, relationTargIndex,
                 sentenceStr, sentencePOSTags.mkString("-")
               )
             } else {
               // by default, if this is not one of the cases above, this program discards this
               // syntactical relationship
               MeaningfulRelationship("", "", 0, "", "", "", 0, "", "")
             }
           }

           def dealWithDObjSyntacticRelation: MeaningfulRelationship = {

             if (isPOSSemantVerb(tagWNPOSSrc) && isPOSSemantNoun(tagWNPOSTarg)) {
               // convert the stems of the source and target to their unique lemma base
               val semantSrc = getLemma(stemSrc, tagWNPOSSrc)
               val semantTarg = getLemma(stemTarg, tagWNPOSTarg)
               // this program does not interpret all the verbs in English yet
               val meaningfulVerbs = List("love", "like", "try", "get", "recommend", "want",
                                          "order", "ask", "eat", "drink", "avoid", "enjoy")
               // TODO: use WordNet to get the synonyms of the meaningful verbs above
               // We use the WordNet lemmas first (i.e., the verb without derivation)
               if (meaningfulVerbs.contains(semantSrc) ||
                   meaningfulVerbs.contains(stemSrc.toLowerCase) ||
                   meaningfulVerbs.contains(relationSrcWord.toLowerCase)) {
                 // we care about this Direct-Object (DObj) syntactical relationship
                 MeaningfulRelationship(
                   relationSrcWord, semantSrc, relationSrcIndex,
                   relationType,
                   relationTargWord, semantTarg, relationTargIndex,
                   sentenceStr, sentencePOSTags.mkString("-")
                 )
               } else {
                 // we ignore this Direct-Object (DObj) syntactical relationship
                 MeaningfulRelationship("", "", 0, "", "", "", 0, "", "")
               }
             } else {
               // by default, if this is not one of the cases above, this program discards this
               // syntactical relationship
               MeaningfulRelationship("", "", 0, "", "", "", 0, "", "")
             }
           }

           if (relationType == "nsubj") { dealWithNSubjSyntacticRelation }
           else if (relationType == "amod") { dealWithAModSyntacticRelation }
           else if (relationType == "compound") { dealWithCompoundSyntacticRelation }
           else if (relationType == "dobj") { dealWithDObjSyntacticRelation }
           else {
             // The program does not interpret other syntactic relationships yet
             MeaningfulRelationship("", "", 0, "", "", "", 0, "", "")
           }
         }
         case _ => MeaningfulRelationship("", "", 0, "", "", "", 0, "", "")
       }
       semanticTopics.appendAll(newSemantTopic.filter {
         case semTopic: MeaningfulRelationship => (semTopic.relation != "")
         case _ => false
       })
    }
    semanticTopics.toList
  }

  // This is the User-Defined-Function (UDF) to be run inside Apache Spark.
  // This function is properly a "transformational" function in Spark, since it transforms a
  // given dataframe into a new one.

  def semanticTopics = udf { document: String => innerSemanticTopics(document) }


  def getInsightsForBusiness(businessName: String, textAnalysisDF: DataFrame): Unit = {

      // get the business ID, since the Yelp reviews are for businessIds, not for businessNames
      val yelpBusinessId = getBusinessId(businessName)

      val textAnalysisForThisBusinessDF = textAnalysisDF.filter(
                    textAnalysisDF("business_id").eqNullSafe(yelpBusinessId)
                 )

      println(s"Reporting insights for Yelp business-ID: $yelpBusinessId ('$businessName')")

      // count each string key (the keys of the map are the business insights, the value is the
      // number of occurrences in the Apache Spark DataFrame)
      val insights: MutableMap[String, Int] = MutableMap()

      textAnalysisForThisBusinessDF.select("topics").collect.foreach(
        row => {
           // println(row.getAs[Seq[Row]](0))
           row.getAs[Seq[Row]](0).foreach(
             item => item match {
               // see the case class MeaningfulRelationship
               case Row(source: String, sourceLemma: String, sourceIndex: Int,
                        relation: String,
                        target: String, targetLemma: String, targetIndex: Int,
                        sentence: String, coreNlpPOS: String) => {

                          def gatherInsightSummariesForRow: Unit = {
                            var mapKey = s"$relation: $source $target"
                            if (relation == "amod") {
                              // adjetival modificator:
                              //    print target (adjective) before source (noun)
                              mapKey = s"$relation: $target $source"
                            } else if (relation == "compound") {
                              // compound word
                              //    print target (complement noun) before source (main noun)
                              mapKey = s"$relation: $target $source"
                            }

                            if (insights.contains(mapKey)) insights(mapKey) += 1
                            else insights(mapKey) = 1
                          }

                          gatherInsightSummariesForRow
               }

               case _ => println(s"Unknown: $item (${item.getClass})")
             }
           )
        }
      )

      insights.toList.sortBy {_._2}.reverse.
        foreach(insight => println(s"${insight._1} (occurrences: ${insight._2})"))

  }


  def getBusinessId(businessName: String): String = {

    // https://docs.mongodb.com/getting-started/java/client/
    // http://mongodb.github.io/mongo-java-driver/3.3/driver/getting-started/quick-tour/

    val mongoClient: MongoClient = new MongoClient("localhost", 27017)
    val dbYelp: MongoDatabase = mongoClient.getDatabase("yelp")
    val yelpBusinessCollection = dbYelp.getCollection("business")

    // val mongoQuery = Array(Aggregates.`match`(Filters.eq("name", businessName)))
    val mongoQuery = new Document("name", businessName)

    val businessId = yelpBusinessCollection.find(mongoQuery).limit(1)
                     .projection(fields(include("business_id")))
                     .first.get("business_id").asInstanceOf[String]

    businessId
  }


  @transient private val cacheHypernyms: MutableMap[String, List[String]] = MutableMap()

  def wordnetGetHypernyms(noun: String): List[String] = {

    // This matching of nouns (customer's desire) based on the desire's hypernyms (e.g., if
    // you desire "snadwich", its hypernyms can be "snack_food", or if you desire "sushi", its
    // hypernyms can be "dish", for example), this matching is more complicated than a mere
    // exact matching by words, but it could be done by numeric ranking:
    //  Ie., by numeric ranking in Spark dataframe "textAnalysisDF":
    //  - if the desire itself is found in the dataframe, that record has the highest ranking
    //  - if a synonym of the desire is found in the dataframe, that record has lower ranking
    //  - if a hypernym of the desire is found in the dataframe, that record has the lowest ranking
    // )
    // The issue is that this analysis of matching the hypernyms and their associated ranking of
    // their match is better done recursively, and while it can be done in Scala, I'm short of
    // time to deliver this program. (In fact, this analysis of hypernyms in WordNet is done is
    // suggested by Princenton, the authors of Wordnet, to be done in Prolog:
    //
    //    http://wordnet.princeton.edu/wordnet/download/
    //
    // in Scala is possible indeed (list intersection of hypernyms) but I'm short of time. This
    // is why the restriction of the analysis to synonyms only and not to hypernyms.

    if (noun == "") {
      return List()
    }

    val semanticDict = getOrCreateWordnetDict()

    val stems = wordnetStemmer.findStems(noun, WordNetPOS.NOUN)
    if (stems == null || stems.size < 1) {
      System.err.println(s"ERROR: WordNet returned no stem for noun '$noun'")
      return List()
    }

    val mainStem = stems.get(0).toLowerCase
    if (cacheHypernyms.contains(mainStem)) {
      return cacheHypernyms(mainStem)
    }

    val idxWord = semanticDict.getIndexWord(mainStem, WordNetPOS.NOUN)
    if (idxWord == null) {
       System.err.println(s"""ERROR: WordNet returned no index for noun:
                              |'$noun', stem '$mainStem'""".stripMargin)
       return List()
    }

    val wordId = idxWord.getWordIDs().get(0)
    if (wordId == null) {
      System.err.println(s"""ERROR: WordNet returned no wordID for noun:
                            |'$noun', stem '$mainStem'""".stripMargin)
      return List()
    }

    val word = semanticDict.getWord(wordId)
    if (word == null) {
      System.err.println(s"""ERROR: WordNet returned no word for noun:
                            |'$noun', stem '$mainStem'""".stripMargin)
      return List()
    }

    // the original semantic meaning of the given noun
    val wordnetSynset = word.getSynset

    // related (more distant) semantic meanings of the given noun
    val wordnetHypernyms = wordnetSynset.getRelatedSynsets(WordNetPointer.HYPERNYM)

    var hypernyms = new ListBuffer[String]()

    // get the words representative of those related semantic meanings
    for { wordnetHypernym <- wordnetHypernyms } {
      val words = semanticDict.getSynset(wordnetHypernym).getWords
      for( w <- words) {
        hypernyms += w.getLemma
      }
    }

    cacheHypernyms(mainStem) = hypernyms.toList
    cacheHypernyms(mainStem)
  }


  def satisfyACustomerRequest(customerDesire: String, textAnalysisDF: DataFrame): Unit = {

    // we suppose that the desire (request) is for a noun
    val firstDesire = customerDesire.trim.split("\\s")(0).toLowerCase

    val hypernymsDesire = wordnetGetHypernyms(firstDesire)

    val rankingByBusiness: MutableMap[String, Double] = MutableMap()

    textAnalysisDF.select("business_id", "topics").foreach(
        row => {
           val businessId = row.getAs[String](0)
           row.getAs[Seq[Row]](1).foreach(
             item => item match {
               // see the case class MeaningfulRelationship
               case Row(source: String, sourceLemma: String, sourceIndex: Int,
                        relation: String,
                        target: String, targetLemma: String, targetIndex: Int,
                        sentence: String, coreNlpPOS: String) => {

                          def updateRankingsForThisBusiness: Unit = {
                            var ranking = 0.0

                            if (relation == "amod") {
                              // adjetival modificator: in this topic, the source is the noun
                              val topicNoun = sourceLemma.toLowerCase
                              if (topicNoun == firstDesire) {  // an exact match of the desire
                                ranking = 1.0
                              } else {  // try an indirect match by semantic hypernyms
                                val topicHypernyms = wordnetGetHypernyms(topicNoun)
                                val intersection = hypernymsDesire.intersect(topicHypernyms)
                                ranking = (intersection.length) / (1.0 + hypernymsDesire.length)
                              }
                            } else if (relation == "compound") {
                              // compound word: in this topic, the source is the main noun
                              val topicNoun = sourceLemma.toLowerCase
                              if (topicNoun == firstDesire) {  // an exact match of the desire
                                ranking = 1.0
                              } else {  // try an indirect match by semantic hypernyms
                                val topicHypernyms = wordnetGetHypernyms(topicNoun)
                                val intersection = hypernymsDesire.intersect(topicHypernyms)
                                ranking = (intersection.length) / (1.0 + hypernymsDesire.length)
                              }
                            } else if (relation == "dobj") {
                              // verbal direct object: in this topic, the target is the main noun
                              val topicNoun = targetLemma.toLowerCase
                              if (topicNoun == firstDesire) {  // an exact match of the desire
                                ranking = 1.0
                              } else {  // try an indirect match by semantic hypernyms
                                val topicHypernyms = wordnetGetHypernyms(topicNoun)
                                val intersection = hypernymsDesire.intersect(topicHypernyms)
                                ranking = (intersection.length) / (1.0 + hypernymsDesire.length)
                              }
                            }

                            if (rankingByBusiness.contains(businessId)) {
                              rankingByBusiness(businessId) += ranking
                            } else rankingByBusiness(businessId) = ranking
                          }

                          updateRankingsForThisBusiness
               }

               case _ =>
             }
           )
        }

    )

    rankingByBusiness.toList.sortBy {_._2}.reverse.
      foreach(businessRanking =>
                     println(s"${businessRanking._1} (ranking: ${businessRanking._2})"))
  }

 /*
    // Writing data to MongoDB is also easy:
    val mostStarredReviews = sqlContext.sql(s"SELECT user_id, stars FROM my_temp_table WHERE stars > ignoreReviewsBelowStars")
    mostStarredReviews.show()
    MongoSpark.save(mostStarredReviews.write.option("collection", "mostStarredReviews").mode("overwrite"))

    val rdd = sparkSession.sparkContext.loadFromMongoDB()     // Uses the SparkConf for configuration
    // val df = MongoSpark.load(sparkSession)
    // val explicitDF = MongoSpark.load[UserIdStars](sparkSession)()
    // val explicitDF = MongoSpark.load(sparkSession, readConfig).toDF[UserIdStars]
    val explicitDF = MongoSpark.load(sparkSession)

    // rescaledData.select("features", "label").take(7).foreach(println)
 */

}

