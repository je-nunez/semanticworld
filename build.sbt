
name := "SemanticWorld"

version := "0.0.1"

scalaVersion := "2.10.6"

scalacOptions ++= Seq("-unchecked", "-deprecation")

fork in run := true
fork in Test := true

javaOptions in run ++= Seq(
    "-Xms8G", "-Xmx8G", "-XX:+UseG1GC"
)

// remove the [info] preffixes given by SBT
outputStrategy        :=   Some(StdoutOutput)

testOptions in Test += Tests.Argument("-oD")

concurrentRestrictions in Global += Tags.limit(Tags.Test, 1)

val apacheSparkVersion = "1.6.2"
val mongodbDriverVersion = "3.2.2"
val mongodbBsonVersion = "3.3.0"
val mongoSparkConnVersion = "1.0.0"
val stanfordCoreNLPVersion = "3.6.0"
val mitJWIWordNetVersion = "2.2.3"
val googleProtoBuffVersion = "3.0.0"
val scOptVersion = "3.5.0"
val sqliteJdbcVersion = "3.8.11.2"
val slf4jVersion = "1.7.21"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % apacheSparkVersion % "provided",
  "org.apache.spark" %% "spark-mllib" % apacheSparkVersion,

  "org.mongodb" % "mongo-java-driver" % mongodbDriverVersion,

  "org.mongodb" % "bson" % mongodbBsonVersion,

  "org.mongodb.spark" % "mongo-spark-connector_2.10" % mongoSparkConnVersion,

  "edu.stanford.nlp" % "stanford-corenlp" % stanfordCoreNLPVersion,
  "edu.stanford.nlp" % "stanford-corenlp" % stanfordCoreNLPVersion classifier "models",
  "edu.stanford.nlp" % "stanford-parser" % stanfordCoreNLPVersion,

  "edu.mit" % "jwi" % mitJWIWordNetVersion,

  "com.google.protobuf" % "protobuf-java" % googleProtoBuffVersion,

  "com.github.scopt" % "scopt_2.10" % scOptVersion,

  "org.slf4j" % "slf4j-nop" % slf4jVersion,

  "org.apache.commons" % "commons-lang3" % "3.0"
)

resolvers ++= Seq(
  "JBoss Repository" at "http://repository.jboss.org/nexus/content/repositories/releases/",
  "Spray Repository" at "http://repo.spray.cc/",
  "Cloudera Repository" at "https://repository.cloudera.com/artifactory/cloudera-repos/",
  "Akka Repository" at "http://repo.akka.io/releases/",
  "Twitter4J Repository" at "http://twitter4j.org/maven2/",
  "Apache HBase" at "https://repository.apache.org/content/repositories/releases",
  "Twitter Maven Repo" at "http://maven.twttr.com/",
  "scala-tools" at "https://oss.sonatype.org/content/groups/scala-tools",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Second Typesafe repo" at "http://repo.typesafe.com/typesafe/maven-releases/",
  "Mesosphere Public Repository" at "http://downloads.mesosphere.io/maven",
  Resolver.sonatypeRepo("public")
)

