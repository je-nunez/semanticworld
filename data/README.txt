
This is the Princenton WordNet lexical database for English (WordNet 3.0 Copyright 2006 by Princeton University).

It can be downloaded from this URL at Princenton University:

      http://wordnet.princeton.edu/wordnet/download/

For example, for a direct download link:

      http://wordnetcode.princeton.edu/wn3.1.dict.tar.gz

The license is here, which allows the present use of it:

      http://wordnet.princeton.edu/wordnet/license/

