#!/bin/sh

echo "Untar yelp_dataset_challenge_academic_dataset.tar"

tar xf yelp_dataset_challenge_academic_dataset.tar



echo "Importing the Checkin collection into MongoDB"

mongoimport  --drop --host=localhost  --db=yelp --collection=checkin --file=yelp_academic_dataset_checkin.json

echo "Importing the Business collection into MongoDB"

mongoimport  --drop --host=localhost  --db=yelp --collection=business --file=yelp_academic_dataset_business.json

echo "Importing the Tip collection into MongoDB"

mongoimport  --drop --host=localhost  --db=yelp --collection=tip --file=yelp_academic_dataset_tip.json

echo "Importing the User collection into MongoDB"

mongoimport  --drop --host=localhost  --db=yelp --collection=user --file=yelp_academic_dataset_user.json

echo "Importing the Review collection into MongoDB"

mongoimport  --drop --host=localhost  --db=yelp --collection=review --file=yelp_academic_dataset_review.json

